/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author CAMILA
 */
public class personaBEAN implements Serializable {  //aca se hace lo de serializable(objeto que contenga una cadena de bytes)
    //atributos privados que solo deben ser accesados por get o set

    private String nombre;
    private String apellido;
    private String estadoCivil;
    private int edad;

    //constructor sin parametros
    public personaBEAN() {
    }

    public String getNombre() {  //se obtiene el nombre
        return nombre;
    }

    public void setNombre(String nombre) { //se asigna el nombre 
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final personaBEAN other = (personaBEAN) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.apellido, other.apellido)) {
            return false;
        }
        return true;
    }

}
