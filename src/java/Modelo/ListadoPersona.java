/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author CAMILA
 */
public class ListadoPersona implements Serializable {

    private ArrayList<personaBEAN> p;

    public ListadoPersona() {
        this.p = new ArrayList<>();
    }

    public void agregarPersona(personaBEAN q) {
        this.p.add(q);
    }

    public boolean existe(personaBEAN q) {
        for (personaBEAN bEAN : p) {
            if (p.contains(q)) {
                return true;
            }
        }
        return false;
    }
    

    public ArrayList<personaBEAN> getP() {
        return p;
    }

    public void setP(ArrayList<personaBEAN> p) {
        this.p = p;
    }

    public int getTamanio() {
        return p.size();
    }

}
