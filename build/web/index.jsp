<%-- 
    Document   : index
    Created on : 25-nov-2020, 19:25:11
    Author     : CAMILA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="style.css">
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,800,900" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <title>Personas</title>
    </head>
    <body>
        <h1 style="margin: 40px 0 0 540px">Registrar persona</h1>
        
        <form action="Vistas/crearPersona.jsp"> 
            <div class="form">
                
            <div class="grupo">
            <p> Nombre: <input type="text" name="nombre" class="barra"/>
            </div>               
               
            <div class="grupo">
            <p> Apellido:   <input type="text" name="apellido" class="barra"/>
            </div>
                
            <div class="grupo">
            <p> Estado Civil: <input type="text" name="estadoCivil" class="barra"/>
            </div>
                
            <div class="grupo">
            <p> Edad: <input type="number" name="edad" class="barra"/><br>
            </div>
                
                <input type="submit" value="Enviar" style="margin: 0 0 10px 160px" class="mover"/>   
          </div>
        </form>
    </body>
</html>
