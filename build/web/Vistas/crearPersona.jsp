<%-- 
    Document   : crearPersona
    Created on : 24-nov-2020, 22:31:38
    Author     : CAMILA
--%>

<%@page import="Modelo.personaBEAN"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../style.css">
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,800,900" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <title>Creando la persona</title>
    </head>
    <body>
        <h1 style="margin: 40px 0 0 540px">Lista de personas</h1><br>   
        <%--con el id operamos los metodos de la clase persona--%>
        <jsp:useBean id="miPersona" class="Modelo.personaBEAN"/> <%-- sirve para construir un nuevo bean--%>
        <jsp:setProperty name="miPersona" property="*" /><%-- sirve para modificar un atributo--%>
        <jsp:useBean id="listadoPersonas" class="Modelo.ListadoPersona" scope="session"/> <%-- sirve para construir un nuevo bean--%>
        
        <table style="margin: 0 0 0 500px" class="table table-striped table-inverse table-responsive mt-3">
            <thead class="table table-dark">
                <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Estado Civil</th>
                    <th>Edad</th>
                </tr>
            </thead>
            <tbody thead-light>
                <%
                    if (!listadoPersonas.existe(miPersona)) {
                        listadoPersonas.agregarPersona(miPersona);
                    }
                    for (int i = 0; i < listadoPersonas.getTamanio(); i++) {
                %>
                <tr class="table-secondary">
                    <td><%= listadoPersonas.getP().get(i).getNombre()%></td>
                    <td><%= listadoPersonas.getP().get(i).getApellido()%></td>
                    <td><%= listadoPersonas.getP().get(i).getEstadoCivil()%></td>
                    <td><%= listadoPersonas.getP().get(i).getEdad()%></td>
                </tr>
                <%}%>
            </tbody>
        </table>
            <a href="../index.jsp"><input type="submit" class="mover2" value="Regresar"></a>
    </body>
</html>
